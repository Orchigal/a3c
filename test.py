import math
import numpy as np
import gym
import time

import torch
import torch.nn.functional as F
import torch.optim as optim
from model import ActorCritic
from torch.autograd import Variable
import logging
logging.basicConfig(filename='A3C_Performance.log',level=logging.DEBUG)


def prepro(I):
    # preprocess the raw observation, turn into 80*80 array
    I = I[35:195] # crop
    I = I[::2,::2,0] # downsample by factor of 2
    I[I == 144] = 0 # erase background (background type 1)
    I[I == 109] = 0 # erase background (background type 2)
    I[I != 0] = 1 # everything else (paddles, ball) just set to 1
    I = I.reshape(-1,1,80,80).astype('float32')
    I = torch.from_numpy(I)
    return I

def test(rank, args, shared_model):
    torch.manual_seed(args.seed + rank)

    env = gym.make(args.env_name)
    env.seed(args.seed + rank)

    model = ActorCritic()

    model.eval()

    state = env.reset()
    state = prepro(state)
    reward_sum = 0
    done = True

    episode_length = 0
    while True:
        episode_length += 1
        # Sync with the shared model
        if done:
            model.load_state_dict(shared_model.state_dict())
            cx = Variable(torch.zeros(1, 512), volatile=True)
            hx = Variable(torch.zeros(1, 512), volatile=True)
        else:
            cx = Variable(cx.data, volatile=True)
            hx = Variable(hx.data, volatile=True)

        value, logit, (hx,cx) = model((Variable(state, volatile=True),(hx,cx)))
        prob = F.softmax(logit)
        action = prob.max(1)[1].data.numpy()
        
        #print(action)
        state, reward, done, _ = env.step(action[0])
        done = done or episode_length >= args.max_episode_length
        reward_sum += reward

        if done:
            msg = 'Episode Reward: ' + str(reward_sum) + ', Episode Length: ' + str(episode_length)
            print(msg)
            logging.info(msg)
            reward_sum = 0
            episode_length = 0
            state = env.reset()
            time.sleep(60)
            torch.save(model.state_dict(),'A3C_Pong.pth')

        state = prepro(state)
